# Cloud control for developers demo

## Requirements
* Java 11
* Maven
* AWS CDK

## Commands
Deployment: ./bin/deploy.sh

Integration tests: ./bin/integration_test.sh
