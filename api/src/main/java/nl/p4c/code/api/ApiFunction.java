package nl.p4c.code.api;

import nl.p4c.code.api.model.RequestBody;

import java.util.function.Function;

public class ApiFunction implements Function<RequestBody, String> {

    @Override
    public String apply(RequestBody requestBody) {
        return new StringBuilder(requestBody.getName()).reverse().toString();
    }
}
