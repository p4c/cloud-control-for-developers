package nl.p4c.code.api;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.p4c.code.api.model.RequestBody;

import java.util.function.Function;

public class AwsHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final Function<RequestBody, String> apiFunction;

    public AwsHandler() {
        this.apiFunction = new ApiFunction();
    }

    public AwsHandler(Function<RequestBody, String> apiFunction) {
        this.apiFunction = apiFunction;
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent apiGatewayProxyRequestEvent, Context context) {
        String body = apiGatewayProxyRequestEvent.getBody();

        RequestBody requestBody;
        try {
            requestBody = new ObjectMapper().readValue(body, RequestBody.class);
        } catch (JsonProcessingException e) {
            return new APIGatewayProxyResponseEvent().withStatusCode(400).withBody("Incorrect request body");
        }

        String output = apiFunction.apply(requestBody);

        return new APIGatewayProxyResponseEvent().withStatusCode(200).withBody(output);
    }
}
