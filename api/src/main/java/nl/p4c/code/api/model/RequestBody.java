package nl.p4c.code.api.model;

public class RequestBody {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
