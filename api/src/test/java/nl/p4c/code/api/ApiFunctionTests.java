package nl.p4c.code.api;

import nl.p4c.code.api.model.RequestBody;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApiFunctionTests {

    ApiFunction apiFunction;

    @BeforeEach
    void setup() {
        apiFunction = new ApiFunction();
    }

    @Test
    void applyReversesRequestBodyName() {
        // Given
        RequestBody requestBody = new RequestBody();
        requestBody.setName("TEST");

        // When
        String output = apiFunction.apply(requestBody);

        // Then
        assertEquals("TSET", output);
    }
}
