package nl.p4c.code.api;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import nl.p4c.code.api.model.RequestBody;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;


class AwsHandlerTests {

    private static final String MOCKED_RESPONSE_NAME = "TESTING";

    private AwsHandler awsHandler;
    private RequestBody capturedBody;

    @BeforeEach
    public void setup() {
        Function<RequestBody, String> function = (body) -> {
            capturedBody = body;
            return MOCKED_RESPONSE_NAME;
        };

        awsHandler = new AwsHandler(function);
    }

    @Test
    void handleRequestAcceptsApiGatewayEventRequest() {
        // Given
        String name = "Ilia";

        APIGatewayProxyRequestEvent apiGatewayProxyRequestEvent = new APIGatewayProxyRequestEvent()
                .withBody(String.format("{\"name\": \"%s\"}", name));

        // When
        awsHandler.handleRequest(apiGatewayProxyRequestEvent, null);

        // Then
        assertEquals(name, capturedBody.getName());
    }

    @Test
    void handleRequestReturnsBadRequestOnIncorrectRequestBody() {
        APIGatewayProxyRequestEvent apiGatewayProxyRequestEvent = new APIGatewayProxyRequestEvent()
                .withBody("any string");

        APIGatewayProxyResponseEvent responseEvent = awsHandler.handleRequest(apiGatewayProxyRequestEvent, null);

        assertEquals(400, responseEvent.getStatusCode());
    }
}
