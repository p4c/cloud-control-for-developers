#!/bin/bash

mvn package

(
  cd infra || exit 1
  cdk deploy
)
