#!/bin/bash
set -e

OUTPUT_FILE="./target/outputs.json"

mvn package

(
  cd infra || exit
  cdk deploy --outputs-file ${OUTPUT_FILE} --require-approval="never"
)

restApiUrl=$(jq -r ".InfraStack.RestApiUrl" "./infra/target/outputs.json")

mvn failsafe:integration-test -pl integration -DrestApiUrl="${restApiUrl}"

(
  cd infra || exit
  cdk destroy -f
)
