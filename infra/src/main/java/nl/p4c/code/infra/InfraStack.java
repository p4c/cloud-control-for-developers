package nl.p4c.code.infra;

import software.amazon.awscdk.core.*;
import software.amazon.awscdk.services.apigateway.LambdaRestApi;
import software.amazon.awscdk.services.apigateway.StageOptions;
import software.amazon.awscdk.services.budgets.CfnBudget;
import software.amazon.awscdk.services.cloudwatch.Alarm;
import software.amazon.awscdk.services.cloudwatch.Metric;
import software.amazon.awscdk.services.cloudwatch.TreatMissingData;
import software.amazon.awscdk.services.cloudwatch.actions.SnsAction;
import software.amazon.awscdk.services.lambda.Code;
import software.amazon.awscdk.services.lambda.Function;
import software.amazon.awscdk.services.lambda.Runtime;
import software.amazon.awscdk.services.sns.Topic;
import software.amazon.awscdk.services.sns.subscriptions.EmailSubscription;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class InfraStack extends Stack {
    public InfraStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    public InfraStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

        Function apiFunction = Function.Builder.create(this, "CloudControlApiFunction")
                .runtime(Runtime.JAVA_11)
                .timeout(Duration.seconds(15))
                .memorySize(512)
                .code(Code.fromAsset("../api/target/api-0.1.jar"))
                .handler("nl.p4c.code.api.AwsHandler")
                .reservedConcurrentExecutions(10)
                .build();

        LambdaRestApi lambdaRestApi = LambdaRestApi.Builder.create(this, "CloudControlApi")
                .handler(apiFunction)
                .deployOptions(StageOptions.builder()
                        .throttlingBurstLimit(10.0)
                        .throttlingRateLimit(10)
                        .build())
                .build();

        constructAlarms(apiFunction);
        constructForecastedCostBudget();

        constructOutputs(lambdaRestApi);
    }

    private void constructAlarms(Function apiFunction) {
        Topic alarmTopic = Topic.Builder.create(this, "EmailTopic")
                .topicName("ApiAlarmTopic")
                .displayName("API Alarm")
                .build();

        alarmTopic.addSubscription(EmailSubscription.Builder.create("ilia.awakimjan@profit4cloud.nl")
                .build());

        SnsAction snsAction = new SnsAction(alarmTopic);

        Metric metric = apiFunction.metric("Invocations");

        Alarm alarm = Alarm.Builder.create(this, "InvocationAlarm")
                .metric(metric)
                .threshold(100)
                .evaluationPeriods(1)
                .treatMissingData(TreatMissingData.IGNORE)
                .actionsEnabled(true)
                .build();

        alarm.addAlarmAction(snsAction);
    }

    private void constructForecastedCostBudget() {
        CfnBudget cfnBudget = CfnBudget.Builder.create(this, "MyBudget")
                .budget(CfnBudget.BudgetDataProperty.builder()
                        .budgetName("MyBudget")
                        .budgetType("COST")
                        .timeUnit("MONTHLY")
                        .costTypes(CfnBudget.CostTypesProperty.builder()
                                .includeCredit(false)
                                .build())
                        .budgetLimit(CfnBudget.SpendProperty.builder()
                                .amount(200)
                                .unit("USD")
                                .build())
                        .build())
                .build();

        CfnBudget.SubscriberProperty subscriberProperty = CfnBudget.SubscriberProperty.builder()
                .subscriptionType("EMAIL")
                .address("example@gmail.com")
                .build();

        CfnBudget.NotificationWithSubscribersProperty notificationWithSubscribersProperty = CfnBudget.NotificationWithSubscribersProperty.builder()
                .notification(CfnBudget.NotificationProperty.builder()
                        .thresholdType("PERCENTAGE")
                        .threshold(80)
                        .notificationType("FORECASTED")
                        .comparisonOperator("GREATER_THAN")
                        .build())
                .subscribers(Collections.singletonList(subscriberProperty))
                .build();

        cfnBudget.setNotificationsWithSubscribers(Collections.singletonList(notificationWithSubscribersProperty));
    }

    private void constructOutputs(LambdaRestApi lambdaRestApi) {
        CfnOutput.Builder.create(this, "RestApiId")
                .value(lambdaRestApi.getRestApiId())
                .exportName("RestApiId")
                .build();

        CfnOutput.Builder.create(this, "RestApiUrl")
                .value(lambdaRestApi.getUrl())
                .exportName("RestApiUrl")
                .build();
    }
}
