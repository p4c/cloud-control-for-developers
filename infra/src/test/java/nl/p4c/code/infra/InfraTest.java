package nl.p4c.code.infra;

import org.junit.jupiter.api.Test;
import software.amazon.awscdk.core.App;

import static org.junit.jupiter.api.Assertions.assertAll;

class InfraTest {

    @Test
    void infraStackSynthesizes() {
        App app = new App();

        assertAll(app::synth);
    }
}
