package nl.p4c.code.integration;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InfraIT {

    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    @Test
    void postRequest() throws URISyntaxException, IOException, InterruptedException {
        // Given
        String restApiUrl = System.getProperty("restApiUrl");
        String name = "Ilia";

        HttpRequest postRequest = HttpRequest.newBuilder()
                .uri(new URI(restApiUrl))
                .POST(HttpRequest.BodyPublishers.ofString(String.format("{\"name\": \"%s\"}", name)))
                .build();

        // When
        HttpResponse<String> response = httpClient.send(postRequest, HttpResponse.BodyHandlers.ofString());

        // Then
        assertEquals(200, response.statusCode());
        assertEquals("ailI", response.body());
    }
}
